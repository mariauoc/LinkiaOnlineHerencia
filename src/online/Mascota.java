/*
 * Entidad Mascota es de tipo Ente
 */
package online;

/**
 *
 * @author mfontana
 */
public class Mascota implements Ente {
    
    private String tipo;
    private double entrenamiento;
    private EnteInteligente cuidador;

    public Mascota(String tipo, double entrenamiento, EnteInteligente cuidador) {
        this.tipo = tipo;
        this.entrenamiento = entrenamiento;
        this.cuidador = cuidador;
    }
    

    public EnteInteligente getCuidador() {
        return cuidador;
    }

    public void setCuidador(EnteInteligente cuidador) {
        this.cuidador = cuidador;
    }

    public double getEntrenamiento() {
        return entrenamiento;
    }

    public void setEntrenamiento(double entrenamiento) {
        this.entrenamiento = entrenamiento;
    }


    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String obtenerNombre() {
        return tipo + " " + cuidador.obtenerNombre();
    }

    @Override
    public double calcularPoder() {
        return cuidador.calcularPoder() + entrenamiento;
    }

}
