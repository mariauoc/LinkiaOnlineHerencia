/*
 * Ser Inteligente, hereda de Ente
 */
package online;

/**
 *
 * @author mfontana
 */
public interface EnteInteligente extends Ente {
    
    public double calcularInteligencia();
}
