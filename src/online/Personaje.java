/*
 * Entidad Personaje , es EnteInteligente (y Ente)
 */
package online;

/**
 *
 * @author mfontana
 */
public class Personaje implements EnteInteligente {

    private String nick;
    private double ataque;
    private double defensa;

    public Personaje(String nick, double ataque, double defensa) {
        this.nick = nick;
        this.ataque = ataque;
        this.defensa = defensa;
    }

    public double getDefensa() {
        return defensa;
    }

    public void setDefensa(double defensa) {
        this.defensa = defensa;
    }

    public double getAtaque() {
        return ataque;
    }

    public void setAtaque(double ataque) {
        this.ataque = ataque;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    @Override
    public double calcularInteligencia() {
        return (ataque - defensa) * 6;
    }

    @Override
    public String obtenerNombre() {
        return nick;
    }

    @Override
    public double calcularPoder() {
        return ataque / defensa;
    }

}
