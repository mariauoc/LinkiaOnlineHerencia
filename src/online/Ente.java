/*
 * Padre de todos
 */
package online;

/**
 *
 * @author mfontana
 */
public interface Ente {

    // Método que devolverá el nombre del objeto
    public String obtenerNombre();

    // Método que devuelve el poder el objeto
    public double calcularPoder();
}
