/*
 * Enunciado del ejercicio
 */
package online;

import java.util.ArrayList;

/**
 *
 * @author mfontana
 */
public class Online {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<Ente> seres = new ArrayList<>();
        Usuario u1 = new Usuario("Usuario", "Martinez", 120, 1.40);
        Personaje p1 = new Personaje("Batman", 100, 35);
        Mascota m1 = new Mascota("Unicornio", 12, u1);
        Mascota m2 = new Mascota("Murciélago", 18, p1);
        
        seres.add(u1);
        seres.add(m1);
        seres.add(p1);
        seres.add(m2);
        
        for (Ente e : seres) {
            System.out.println("Nombre: "+e.obtenerNombre()+ " Poder: "+e.calcularPoder());
            if (e instanceof EnteInteligente) {
                EnteInteligente i = (EnteInteligente) e;
                System.out.println("Inteligencia: "+i.calcularInteligencia());
            }
        }
    }
    
}
