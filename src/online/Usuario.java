/*
 * Entidad Usuario es SerInteligente y Ser
 */
package online;

/**
 *
 * @author mfontana
 */
public class Usuario implements EnteInteligente {

    private String nombre;
    private String apellidos;
    private double peso;
    private double altura;

    public Usuario(String nombre, String apellidos, double peso, double altura) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.peso = peso;
        this.altura = altura;
    }
    
    

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public double calcularInteligencia() {
        return (peso + altura) * 8;
    }

    @Override
    public String obtenerNombre() {
        return nombre + " " + apellidos;
    }

    @Override
    public double calcularPoder() {
        return peso / altura;
    }

}
